Charte graphique
=================

Ce site est disponible à l'adresse http://etu-ajabep.tk/eweb-rock/

Cette charte est en partie inspiré de http://stylifyme.com/?stylify=etu-ajabep.tk%2Feweb-rock%2F

Couleurs
--------
Nous avons choisi des couleurs respectant la norme `WCAG 2.0`.

Couleur     | Code hexadécimal  | Raison
----------- | :---------------: | :-----------------------------------------------------------------------
Noir        |      #000000      | Utilisé pour respecter l'accessibilité web selon la norme `WCAG 2.0`
Bleu marine |      #07141e      | Utilisé dans le lecteur média pour une immersion dans le sujet : le rock
Gris        |      #333333      | Utilisé pour le signe d'invitation à scroller dans la première section
Bleu        |      #4cb1ff      | Utilisé pour signalé les zones cliquables
Gris        |      #666666      | Utilisé pour le fond des sections pour une immersion dans le sujet
Gris        |      #cccccc      | Utilisé dans le lecteur média
Blanc       |      #ffffff      | Permet d'avoir un contraste suffisant pour une lecture aisé
Turquoise   |      #aaeeff      | Signale les liens


Polices
-------
Nous n'avons choisi que des polices sans serif. En effet, elles permettent d'augmenter la lisibilité sur un écran. Nous avons choisi la police `sans-sérif` par défaut. Celle-ci a l'avantage d'être présente par défaut sur tout les navigateurs.
Cette dernière est utilisé en 32px et en 16px.


Logotype
--------
Le logo est un jeu de mot avec l'expression "You rock". Il est fait avec la police de caractère `Blazed` (trouvable gratuitement sur [dafont.com](http://www.dafont.com) et est fait avec la couleur noir (`#000`). On peut donc le mettre sur des couleurs clair, avec lesquelles la norme `WCAG 2.0` serait respectée.


UI/UX
------

### transitions
De façon à moduler l'interface sans changement brusque, nous avons intégrer des transitions. Ces transitions doivent s'adapter en fonction de l'effet voulu. Pour cela nous avons créé 3 durée de transitions. Toutefois, nous avons privilégié la plus courtes pour ne pas gêner l’utilisateur.

* 0,2s       transition standard
* 0,3s       transition moyenne
* 0,5s       transition lente


### transparence
La section 1 possède de la transparence de 20%. Cela permet de voir la vidéo en arrière plan, et ainsi d'augmenter l’intérêt du site pour l'utilisateur.


