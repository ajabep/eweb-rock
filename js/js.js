/*jslint browser: true, evil: false, plusplus: true, unparam: false, sloppy: false, indent: 4*/
var HEIGHT_NAV = 80; // taille du menu
var TRANSITION_DURATION = 0.4; // durée de la transition voulue en JS
var KEYCODE_ESC = 27; // code touche echap

var jsonParsed, // vas stoquer les données à afficher
    vinyleContainer; // vas stoquer la section qui inclut le vinyle


$('html').addClass('js'); // pour la compatibilité sans JS


function resize() { // executé lorsqu'on redimentionne la fenetre
//-    // var height = $('.moreSign').css('height'); //

//-    // $('.resize').css('height', Math.max(window.innerHeight, 400));

//-    // $('#myVideo1').css('height', Math.max(window.innerHeight - parseInt(height), 400 - parseInt(height)));
	$('#myVideo1').css('height', window.innerHeight);

    $('[data-height]').each(function () { // redimentionne tous les element que je n'arrive pas redimantionner en css
        var $this = $(this),
			dataHeight = $this.attr('data-height'),
            heightWanted = parseInt(dataHeight),
            ext = dataHeight.substring(heightWanted.toString().length),
			calculatedHeight;

        switch (ext) {
            case '%':
                calculatedHeight = window.innerHeight * heightWanted / 100;

            // you can do your extention
        }

		if($this.attr('data-max-height') == "")
			$this.css('height', calculatedHeight);
		else
			$this.css('height', Math.max(calculatedHeight, $this.attr('data-max-height')));

    });
}

function showBandText(groupName) { // pour la section groupe
	var text = jsonParsed.personalites[groupName];
	window.setTimeout(
		function (elem, txt) {
			elem
			  .find('.text')
			  .html(text);
			elem.addClass('open');
		},
		TRANSITION_DURATION * 1000,
		$('.vinyle').parent().find('.more').removeClass('open'),
		text
	);
}

function showBeforePers() { // affiche une personnalité qui avait la classe css "before"
	var beforeElem = vinyleContainer.find('.before');
	if (beforeElem.length) {

		$('.vinyle .arrow-down').removeClass('hidden');

		var afterElem = vinyleContainer.find('.after');

		afterElem.addClass('afterAfter').removeClass('after'); // la pers en "after" passe en "afterAfter" (donc caché)


    	vinyleContainer.find('.current').removeClass('current').addClass('after'); // le current passe en after
		showBandText(beforeElem.addClass('current').removeClass('before').html());

    	var beforeBeforeElem = vinyleContainer.find('.beforeBefore');

    	if (beforeBeforeElem.length) {
      		$(beforeBeforeElem[beforeBeforeElem.length - 1]).addClass('before').removeClass('beforeBefore'); // on passe le dernier element des "beforeBefore" (don caché) en "before" s'il exeiste
    	}
    	else {
      		$('.vinyle .arrow-up').addClass('hidden'); // sinon, on cache la fleche vers le haut
    	}
  	}
}

function showAfterPers() { // affiche une personnalité qui avait la classe css "after"
	var afterElem = vinyleContainer.find('.after');
  	if (afterElem.length) {

		var beforeElem = vinyleContainer.find('.before')

		beforeElem.addClass('beforeBefore').removeClass('before'); // On fait passer la personne qui a la classe css `before` (on le cache)
		$('.vinyle .arrow-up').removeClass('hidden');
		vinyleContainer.find('.current').removeClass('current').addClass('before'); // Le current passe en before
		showBandText(afterElem.addClass('current').removeClass('after').html()); // Le after passe en current

    	var aferAfterElem = vinyleContainer.find('.afterAfter');

    	if (aferAfterElem.length) {
			$(aferAfterElem[0]).addClass('after').removeClass('afterAfter'); // on passe le permier des élements  "after" en "afterAfter" (caché) si possible
    	}
    	else {
      		$('.vinyle .arrow-down').addClass('hidden'); // sinon, n cache la fleche vers le bas
    	}
  	}
}


$(function () {
	var batterieContainer = $('.batterie'),
        i = 0,
        classHTML;
    window.vinyleContainer = $('.vinyle').find('ul');
    window.jsonParsed = JSON.parse(json);

    for (var groupe in jsonParsed.groupes || {}) {
        batterieContainer.append('<span class="zone">' + groupe + '</span>');
    }

    for (var personalite in jsonParsed.personalites || {}) {
        if ( !i ) {
            classHTML = 'current';
        }
        else if ( !(i - 1) ) {
            classHTML = 'after';
        }
        else {
            classHTML = 'afterAfter';
        }

        vinyleContainer.append('<li class="' + classHTML + '">' + personalite + '</li>');

        if ( !i ) {
            showBandText( personalite );
        }

      	++i;
    }

    $('#myVideo1').removeAttr('muted');

    videojs('myVideo1', {}, function () {
        this.muted(1);

        $(this.I).on('ended', function () {
			this.playlistChoose(playlist, playlist.playing);
        });

        $(this.b)
          .find('.vjs-control-bar')
          .find('.vjs-fullscreen-control.vjs-control')
          .click(function () {
            $('#myVideo1').removeClass('vjs-fullscreen');
            videojs.players.myVideo1.muted(1);
            videojs.players.myVideo1.isFullscreen(0);
          });
    });

	videojs.players.myVideo1.I.playlistChoose = function (playlist, index) {
		var urlNow = playlist.urls[playlist.playing - 1],
			urlNext,
			descriptionMusic = $('#descriptionMusic'),
			ext = this.src.slice(this.src.search(urlNow.src) + urlNow.src.length);


		if (playlist.urls.length <= index)
			index = 0;


		urlNext = playlist.urls[ index ];

		playlist.playing = index +1;

		this.src = urlNext.src + ext;

		$(this)
		  .find('source')
		  .each(function () {
			var ext = this.src.slice(this.src.search(urlNow.src) + urlNow.src.length);

			this.src = urlNext.src + ext;
		  });



		descriptionMusic.find('.title').text(urlNext.title);
		descriptionMusic.find('.author').text(urlNext.author);
		descriptionMusic.find('.year').text(urlNext.year);

		this.play();
	}

	videojs.players.myVideo1.I.playlistChoose.apply(videojs.players.myVideo1.I, [playlist, 0]);

    $('#section1')
      .find('.play')
      .click(function () {
          $('#myVideo1').addClass('vjs-fullscreen');
          videojs.players.myVideo1.muted(0);
          videojs.players.myVideo1.isFullscreen(1);
		  $('html, body').on('keydown', function(e){
		 		if (e.keyCode == KEYCODE_ESC) {
					$('#section1')
      					.find('.exitFullscreen')
      					.trigger('click');
				}
		  });
      });

    $('#section1')
      .find('.exitFullscreen')
      .click(function () {
          $('#myVideo1').removeClass('vjs-fullscreen');
          videojs.players.myVideo1.muted(1);
          videojs.players.myVideo1.isFullscreen(0);
		  $('html, body').off('keydown');
      });

    $(window)
      .resize(function () {
          resize();
      })
      .trigger('resize');

    $('.vinyle .arrow-up').click(function() {
      showBeforePers();
    });

    $('.vinyle .arrow-down').click(function() {
      showAfterPers();
    });

    $('.vinyle li').click(function() {
      if( $(this).is( vinyleContainer.find('.before') ) )
        showBeforePers();
      else if( $(this).is( vinyleContainer.find('.after') ) )
        showAfterPers();
    });

    $('.batterie .zone').click(function (e) {
        var text = jsonParsed.groupes[$(this).html()];
        window.setTimeout(
            function (elem, txt) {
                elem
				   .find('.text')
				   .html(text);
                elem.addClass('open');
            },
            TRANSITION_DURATION * 1000,
            $('.batterie').parent().find(' .more').removeClass('open'),
            text
        );
    });

    $('[data-href]').click(function () { // les liens pour se déplacer à l'intérieur de la page
		scrollTo({
			topPosition: $($(this).attr('data-href')).offset().top - HEIGHT_NAV,
        	transitionDuration : TRANSITION_DURATION * 1000
    	});
    });

    $('[id^=section]')
      .on('scrollSpy:exit', function () { // qd une section sort de l'écran
        $('nav').find('ul').find('[data-href=#' + $(this).attr('id') + ']').removeClass('active');
        if ($(this).attr('id') == 'section1') {
            $('nav').addClass('open');
        }
      })
      .on('scrollSpy:enter', function () { // qd une section entre sur l'écran
        $('nav').find('ul').find('[data-href=#' + $(this).attr('id') + ']').addClass('active');
        if ($(this).attr('id') == 'section1') {
            $('nav').removeClass('open');
        }
      })
	  .scrollSpy({'offsetTop': HEIGHT_NAV});
});

function playlistObj (urls) { // Playlist object
	this.playing = 1;
	this.urls = urls;

	this.randomize = function() {
		var musicIndex,
			urls = this.urls,
			urlsAfter,
			srcPlaying = this.urls[this.playing - 1].src,
			newPlayingIndex;

		this.urls = [];

		for (var i = 0, c = urls.length; i<c; ++i ){
			musicIndex = Math.floor(Math.random() * (c - i));
			this.urls.push(urls[musicIndex]);

			if( srcPlaying == urls[musicIndex].src )
				newPlayingIndex = i + 1;

			urlsAfter = [];
			for(var j = 0, d = urls.length; j<d; ++j) {
				if(j != musicIndex )
					urlsAfter.push(urls[j]);
			}
			urls = urlsAfter;
		}

		this.playing = newPlayingIndex;
	}

	this.randomize();
}



var playlist = new playlistObj ([
	{
		src: 'videos/NOM_DE_LA_VIDEO_SANS_EXTENTION',
		author: 'AUTEUR',
		title: 'TITRE',
		year: 3695 // année de la vidéo
	}
]);





function scrollTo(option) { // inspired by [anchor.js](https://github.com/cbopp-art/anchor)
	$viewport = $('html, body'); // body to webkit, html to others

	var optionAnimate = {};

	if(option.topPosition)
		optionAnimate.scrollTop = option.topPosition
	if(option.leftPosition)
		optionAnimate.scrollLeft = option.leftPosition;

	option.transitionDuration = option.transitionDuration || 1000;
	option.transitionTimingFunction = option.transitionTimingFunction || 'swing';


	$viewport.animate(
		optionAnimate,
		option.transitionDuration,
		option.transitionTimingFunction
	);

	// Stop the animation immediately, if a user manually scrolls during the animation.
	$viewport.bind('scroll mousedown DOMMouseScroll mousewheel keyup', function (event) {
		if (event.which > 0 || event.type === 'mousedown' || event.type === 'mousewheel') {
			$viewport.stop().unbind('scroll mousedown DOMMouseScroll mousewheel keyup');
		}
	});
}
