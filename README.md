EWEB-Rock
=========

Ce projet à été fait pour l'expression web, une matière enseigné dans le cadre de mes études.

Le code est mal documenté, mais le but n'était pas le coté technique, mais l'interface finale. Je ne prévoyais pas de partager les sources, mais comme un camarade me les a demandé, je les met à disposition du public.

Le code est sous [MIT License](http://choosealicense.com/licenses/mit/).